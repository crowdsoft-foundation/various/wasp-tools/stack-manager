function scrollToId(id) {
    scrollToElement(document.getElementById(id))
}

function scrollToElement(element) {
    element.scrollIntoView({
        behavior: 'smooth'
    })
}

class Installer {
    instance = undefined

    constructor(logContainer) {
        if (Installer.instance)
            return Installer.instance;
        scrollToElement(logContainer)
        this.installationRunning = false
        this.logContainer = logContainer
        this.logEntryCounter = 0
        this.latencies = []
        this.pingStartTime = undefined
        this.notConnectedTimeout = undefined
        this.socket = undefined
        Installer.instance = this;
    }

    connectSocketIO() {
        let namespace = '/';
        Installer.instance.socket = io("localhost:8080" + namespace, {
            path: '/socket.io'
        });

        Installer.instance.socket.on('connect', function () {
            Installer.instance.addLog("Connection to log-server established")
        });

        Installer.instance.socket.on('conPong', function () {
            window.clearTimeout(Installer.instance.notConnectedTimeout)
            let latency = (new Date).getTime() - Installer.instance.pingStartTime
            Installer.instance.latencies.push(latency);
            Installer.instance.latencies = Installer.instance.latencies.slice(-30); // keep last 30 samples
            let sum = 0;
            for (let i = 0; i < Installer.instance.latencies.length; i++)
                sum += Installer.instance.latencies[i];

            $('#ping-pong').text(Math.floor(10 * sum / Installer.instance.latencies.length / 10));
        });

        Installer.instance.socket.on('addLog', function (msg, cb) {
            Installer.instance.addLog(msg.message, msg.id)
        });

        window.setInterval(function () {
            Installer.instance.pingStartTime = (new Date).getTime();
            Installer.instance.socket.emit('conPing');
            Installer.instance.notConnectedTimeout = window.setTimeout(function () {
                $('#ping-pong').text("∞")
            }, 1100)
        }, 1000);
    }

    addLog(message, id= undefined, newBlock = false, titleText = undefined) {
        Installer.instance.logEntryCounter++
        if (newBlock) {
            if (titleText) {
                let title = document.createElement("H2");
                title.innerHTML = titleText
                Installer.instance.logContainer.appendChild(title)
            }

            let separator = document.createElement("HR");
            Installer.instance.logContainer.appendChild(separator)
        }
        let logEntryWrapper = null
        if(id && document.getElementById(id)) {
            logEntryWrapper = document.getElementById(id)
        } else {
            logEntryWrapper = document.createElement("P");
            logEntryWrapper.class = "logEntry"
        }

        if(id) {
            logEntryWrapper.id = id
        } else {
            logEntryWrapper.id = "logEntry_" + Installer.instance.logEntryCounter
        }

        let date = new Date()
        let dateTimeString = date.toLocaleDateString() + " " + date.toLocaleTimeString()

        logEntryWrapper.innerHTML = dateTimeString + ": " + message
        Installer.instance.logContainer.appendChild(logEntryWrapper)
    }

    install() {
        if (!Installer.instance.installationRunning) {
            Installer.instance.connectSocketIO()
            var settings = {
                "url": "http://localhost:8080/start-install",
                "method": "GET",
                "timeout": 0,
            };

            $.ajax(settings).done(function (response) {});
        }
        Installer.instance.installationRunning = true
    }
}

