from main import app
import cherrypy

import os
import cherrypy_cors
cherrypy_cors.install()


from inspect import getframeinfo, stack

if __name__ == '__main__':
    # Mount the application
    cherrypy.tree.graft(app, "/api")

    class Static(object):
        pass

    caller = getframeinfo(stack()[0][0])
    PATH = os.path.abspath(os.path.dirname(caller.filename))
    cherrypy.tree.mount(Static(), "/", config={
        '/': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': PATH + "/html",
            'tools.staticdir.index': 'index.html',
            'cors.expose.on': True
        }
    })

    # Unsubscribe the default server
    cherrypy.server.unsubscribe()

    # Instantiate a new server object
    server = cherrypy._cpserver.Server()

    # Configure the server object
    server.socket_host = "0.0.0.0"
    server.socket_port = 8000
    server.thread_pool = 30

    # Subscribe this server
    server.subscribe()

    # Start the server engine (Option 1 *and* 2)
    cherrypy.engine.start()
    cherrypy.engine.block()